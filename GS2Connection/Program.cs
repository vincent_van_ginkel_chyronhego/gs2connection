﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using GSTK5Lib;

namespace GS2Connection
{
    class Program
    {
        static TitleManager tk5 = new TitleManager();
        static String oldImage = "";

        public static void loadImage( String project, String scene, String image )
        {          
            tk5.setProject(project);
            tk5.getClient().connect();

            IGSTitle title = tk5.createTitle("default");
            CommandList cmds = tk5.createCommandList();

            cmds.loadScene(scene, "scene");
            cmds.set("imageOld", "FileName", oldImage);
            cmds.set("imageNew", "FileName", image);

            var animation = tk5.createAnimation();
            var ch = animation.createChannel();
            ch.destination = "Opacity";
            ch.addKeyframe(0, 1, "FLAT", "FLAT");
            ch.addKeyframe(25, 0, "FLAT", "FLAT");
            cmds.animate(animation, "imageOld");

            title.execute(cmds);

            oldImage = image;
        }

        public static string data = null;
        public static void StartListening(int port, String project, String scene)
        {
            byte[] bytes = new Byte[1024];

            while (true)
            {
                IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
                IPAddress ipAddress = IPAddress.Any;
                IPEndPoint localEndPoint = new IPEndPoint(ipAddress, port);

                Socket listener = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                try
                {
                    listener.Bind(localEndPoint);
                    listener.Listen(10);

                    while (true)
                    {
                        Socket handler = listener.Accept();

                        while (true)
                        {
                            data = null;
                            int bytesRec = handler.Receive(bytes);

                            data = Encoding.ASCII.GetString(bytes, 0, bytesRec);

                            int first = data.IndexOf("loadImage ");
                            int last = data.Length;

                            if (first < last && first >= 0)
                            {

                                first += "loadImage ".Length;

                                string image = data.Substring(first, last - first);
                                image = image.TrimEnd();

                                loadImage(project, scene, image);
                            }
                        }
                        handler.Shutdown(SocketShutdown.Both);
                        handler.Close();
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    listener.Close();
                }
            }

            Console.WriteLine("\nPress ENTER to continue...");
            Console.Read();

        }

        static void Main(string[] args)
        {
            int port = 5000;
            String project = "GStudio2";
            String scene = "default.gse";
            if ( args.Length > 0 )
            {
                port = Convert.ToInt32(args[0]);
            }
            if (args.Length > 1)
            {
                project = args[1];
            }
            if (args.Length > 2)
            {
                scene = args[2];
            }

            Console.WriteLine( "Listening to port: ");
            Console.WriteLine(port);
            StartListening(port, project, scene );
        }       
    }
}
